<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $occurrencesArray = array();
        $file = fopen($filePath, "r");
        while (($buffer = fgets($file, 4096)) !== false) {
            $buffer = str_split(strtolower($buffer));
            foreach ($buffer as $char) {
                if(ctype_alpha($char)) {
                    if(!array_key_exists($char, $occurrencesArray)) $occurrencesArray[$char] = 0;
                    $occurrencesArray[$char]++;
                }
            }
        }
        asort($occurrencesArray);
        return $occurrencesArray;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $lengthOfArray = count($parsedFile);
        $occurrences = array_values($parsedFile)[round($lengthOfArray/2 - 1)];
        return array_keys($parsedFile)[$lengthOfArray/2 - 1];
    }
}